package ejercicio18;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		// Creo objeto clase Scanner
		Scanner ejercicio = new Scanner(System.in);

		// byte
		System.out.println("Introduce un byte");
		byte numero = ejercicio.nextByte();
		System.out.println("el numero byte es " + numero);

		// short
		System.out.println("Introduce un short");
		short numero2 = ejercicio.nextShort();
		System.out.println("el numero short es " + numero2);

		// int
		System.out.println("Introduce un int");
		int numero3 = ejercicio.nextInt();
		System.out.println("el numero int es " + numero3);

		// long
		System.out.println("Introduce un long");
		long numero4 = ejercicio.nextLong();
		System.out.println("el numero long es " + numero4);

		// float
		System.out.println("Introduce un float");
		float numero5 = ejercicio.nextFloat();
		System.out.println("el numero float es " + numero5);

		// double
		System.out.println("Introduce un double");
		double numero6 = ejercicio.nextDouble();
		System.out.println("el numero double es " + numero6);

		// Antes de cambia de numer a caracter hay que limpiar e buffer
		ejercicio.nextLine();

		// char
		// para leer un caracter se un nexLine que lee una linea
		// para guardar solo el primer caracter se tiene que indicar con .charAT(0)
		// nextLine() -> coge toda la linea
		// nextLine() + .charAt(0) -> coge el primer caracter 
		System.out.println("Introduce un char");
		char numero7 = ejercicio.nextLine().charAt(0);
		System.out.println("El caracter es " + numero7
				);
		// Cierro Scanner
		ejercicio.close();

	}

}
