package ejercicio15;

public class Ejercicio15 {
	// Constantes se declaran fuera
	// "final" = constante
	// Si esta fuera de public void deve de llevar "static"
	
	static final String MENSAJE_INICIO="inicio del programa";
	static final String MENSAJE_FIN="fin del programa";

	public static void main(String[] args) {
		System.out.println(MENSAJE_INICIO);
		
		String frase="Erase una vez, en un pais muy muy lejano";
		System.out.println(frase);
		String trozo=frase.substring(6, 25);
		System.out.println(trozo);
		
		System.out.println(MENSAJE_FIN);
		
	}

}
