package ejercicio17;

public class Ejercicio17Bis {

	public static void main(String[] args) {
		String cadena1="A";
		String cadena2="Otra cadena";
		
		//Los string tienen un metod que permite comparar cadenas
		//compareTo
		//El resultado de compareTo es un entero
		//primeraCadena.compareTo(segundaCadena) -> devuleve un entero
		
		int resultado=cadena1.compareTo(cadena2);
		
		//resultado>0 --->cadena1>cadena2
		//resultado<0 --->cadena1< cadena2
		//resultado==0 -->cadena1=cadena2
		// �Compara los codigos ASCI!
		
		System.out.println(resultado==0?"cadena1 es mayor":(resultado==0?"cadena 1 es igual cadena2":"cadena1 es menor"));

	}

}
