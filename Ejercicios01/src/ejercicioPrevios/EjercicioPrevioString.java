package ejercicioPrevios;

public class EjercicioPrevioString {

	public static void main(String[] args) {
		int num1=3; //tipo pimitivo
		System.out.println("El valor de num1 es " + num1);
		char letra ='a'; //tipo primitivo
		System.out.println("La letra es "+ letra);
		String cadena ="hola caracola"; //no es primitivo -> Mayuscula -> clase
		System.out.println("La cadena es " + cadena);
		// Clase String pertenece a java.lang
		//java.lang es un paquete de java
		// Viene po defecto, por eso no necesito import
		System.out.println("La longitud de la cadena es "+ cadena.length());
		
		
		//Declaro un String y lo muestro
		String textoEntero="12345";
		System.out.println(textoEntero);
		int entero=Integer.parseInt(textoEntero);
		entero--;
		System.out.println(entero);
		
		//Los String tienen muchas funcionalidades
		// por ejemplo extrar un trozo de una cadena
		String frase="Esto es una frase completa";
		System.out.println(frase);
		// con substring puedo 
		String trozo=frase.substring(5, 11);
		System.out.println();
		
		

	}

}
