package ejercicioPrevios;

import java.util.Scanner;

public class EjercicioPrevioScaner {

	public static void main(String[] args) {
		// "Scarner" "NombreVariable" = new Scanner(System.in)
		// Necesita un "import java.until.Scanner;"
		Scanner input = new Scanner(System.in);
		
		System.out.println("Dame un numero entero");
		int numInt=input.nextInt(); // nextInt() permite leer conversion letra Scanner a int
		System.out.println("El numero entero es " + numInt);
		//Cierro el Scanner
		input.close();
		
		
	}

}
