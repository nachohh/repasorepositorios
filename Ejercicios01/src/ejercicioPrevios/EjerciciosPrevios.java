package ejercicioPrevios;

public class EjerciciosPrevios {

	public static void main(String[] args) {
		// Declaro una variable entera var1 y le doy valor, lo muestro junto a un texto
		int var1 = 5;
		System.out.println("var1 es " + var1);

		// Declaro una variable double var2 y le doy un valor, lo muestro junto a un
		// texto
		double var2 = 3.1416;
		System.out.println("var2 es " + var2);

		// Declaro una varibale char var3 y le doy una letra, muestro su valor entero
		char var3 = 'A';
		System.out.println("var3 es " + (int) var3);

		// Declaro una variable entera var4 y le incremento uno, muestro ambas variables
		int var4 = 10;
		System.out.println("var4 es " + var4);
		var4++;
		System.out.println("var4 es " + var4);

		// Declaro una varuable double var5 y la casteo a entera
		double var5 = 2.34215;
		System.out.println("El valor de var5 " + var5);
		int var5Entera = (int) var5;
		System.out.println("El valor de var5Entera es " + var5);

		// Declaro una variable double var6 que contenga la suma de var2 y var3
		double var6;
		var6 = var2 + var3;
		System.out.println("var6 es " + var6);

		// Declaro una variable double var7 que contenga la division double de var4 y
		// var5
		double var7; // double var7 = (double) var4 7 var5
		var7 = (double) var4 / var5;
		System.out.println("var7 es " + var7);

		// Declaro un boolean var8 y lo inicializo a true, muestro su valor en un texto
		boolean var8=true;
		System.out.println("El valor de var8 es "+var8);

		// Declaro tres numero, num1=3, num2=5, num3=6
		int num1 = 3;
		int num2 = 5;
		int num3 = 6;

		// Creo un operador condicional que use num1 y num2 y muestr true (Con el
		// operador AND)
		System.out.println((num1 <= 3) && (num2 <= 5)? "num1 es <= 3 y num2 es <=5":"No cumple condiciones");

		// Creo un operador condicianal que uso num2 y num3 y muestre false (Con el
		// operador OR)
		System.out.println((num2 > 10) || (num3 == 25)?"num2 es < 10 y num3 es == 25":"No cumple las condiciones");

		// Muestro meidante un syso y opero un condicional la comprobacion de que num3
		// es par
		System.out.println(num3 %2==0 ? "Es par":"No es par");
		
		// Decrementa la varibale2 2 unidades, muestrala
		System.out.println("valor de antes del decrementeo "+ num2);
		num2-=2;
		System.out.println("Valor despues del decremento "+ num2);

	}

}
