package ejercicio04;

public class Ejercicio04 {

	public static void main(String[] args) {
		int varA = 6;
		int varB = 45;
		int varC = 500;
		int varD = 1234;

		System.out.println(varA);
		System.out.println(varB);
		System.out.println(varC);
		System.out.println(varD);

		// Para intercambiar el valor entre diferentes variables
		// necesitamos como minimo una variables mas
		// para no perder el valor

		int aux = varA;

		// Ahora ya he guardado el primer valor
		// ya puedo cambiar los valores

		varA = varB;
		varB = varC;
		varC = varD;

		// Como varA ya no contiene su valor original
		// la tengo que transpasar usando aux
		varD = aux;

		System.out.println(varA);
		System.out.println(varB);
		System.out.println(varC);
		System.out.println(varD);

	}

}
