package ejercicio12;

public class Ejercicio12 {

	public static void main(String[] args) {
		double decimalDoble=12.23123423;
		float decimalSimple=(float) decimalDoble;
		
		System.out.println(decimalDoble);
		System.out.println(decimalSimple);
		
		System.out.println("He convertido un decimal de 8 bytes");
		System.out.println("en uno de 4 bytes");

	}

}
