package revisar;

public class OperadorCondicional {

	public static void main(String[] args) {
		// Esto es lo que ya sabiamos hacer
		int num1=56;
		int num2=-63;
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num2>num1);
		// NUEVO
		System.out.println(num2>num1?"num1 es menor":"num2 es mayor");
		System.out.println(num1>5?"num1 es mayor que 5":"num1 es mayor o igual a 5");
		
		
		

	}

}
